﻿using CoreAnimation;
using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XamarinStartKit.CustomView;
using XamarinStartKit.iOS.CustomView;

[assembly: ExportRenderer(typeof(InputEntry), typeof(InputEntryRender))]
namespace XamarinStartKit.iOS.CustomView
{
    public class InputEntryRender: EntryRenderer
    {
        public InputEntryRender()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
               Control.BorderStyle = UIKit.UITextBorderStyle.None;
            }
        }
    }
}
