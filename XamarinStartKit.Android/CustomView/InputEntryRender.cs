﻿using System;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XamarinStartKit.CustomView;
using XamarinStartKit.Droid.CustomView;

[assembly: ExportRenderer(typeof(InputEntry), typeof(InputEntryRender))]
namespace XamarinStartKit.Droid.CustomView
{
    public class InputEntryRender: EntryRenderer
    {
        public InputEntryRender(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background = null;
            }
        }
    }
}
