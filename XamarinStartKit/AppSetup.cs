﻿using System;
using Autofac;
using XamarinStartKit.Interface;
using XamarinStartKit.Utiltity;
using XamarinStartKit.ViewModel;

namespace XamarinStartKit
{
    public class AppSetup
    {
        public IContainer CreateContainer()
        {
            var containerBuilder = new ContainerBuilder();
            RegisterDependencies(containerBuilder);
            return containerBuilder.Build();
        }

        protected virtual void RegisterDependencies(ContainerBuilder cb)
        {
            //// Register Services
            cb.RegisterType<LoadingIndicator>().As<ILoadingIndicator>().SingleInstance();
            cb.RegisterType<ReadFile>().As<IReadFile>().SingleInstance();
            cb.RegisterType<MediaUtility>().As<IMediaUtility>().SingleInstance();
            cb.RegisterType<XamarinStartKit.Utiltity.AppContext>().As<IAppContext>().SingleInstance();
            
            //// Register View Models
            cb.RegisterType<UserProfileSettingsViewModel>().AsSelf();
            cb.RegisterType<MainViewModel>().AsSelf();
            cb.RegisterType<UserViewModel>().AsSelf();
            cb.RegisterType<TinderUserMainPageViewModel>().AsSelf();
            
        }
    }
}
