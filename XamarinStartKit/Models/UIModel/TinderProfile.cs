﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinStartKit.Models.UIModel
{
    public class TinderProfile
    {
        public int ProfileId { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public Gender Gender { get; set; }

        public string Photo { get; set; }
    }
    public enum Gender
    {
        Female = 1,

        Male = 2
    }
}
