﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinStartKit.Models.UIModel
{
    public class UserProfileUIModel
    {
        public string UserName { get; set; }
        public List<string> SpokenLanguages { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Description { get; set; }
        public List<InterestUIModle> Interests { get; set; }
    }

    public partial class InterestUIModle
    {
        public string Action { get; set; }
        public string Something { get; set; }
    }
}

