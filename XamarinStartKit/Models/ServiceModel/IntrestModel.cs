﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinStartKit.Models.ServiceModel
{
    public class IntrestModel
    {
        [JsonProperty("enum")]
        public string Enum { get; set; }

        [JsonProperty("translation")]
        public string Translation { get; set; }

    }
}
