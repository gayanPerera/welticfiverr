﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinStartKit.Models.ServiceModel
{
    public class UserProfile
    {
        [JsonProperty("smallProfileImageUrl")]
        public string SmallProfileImageUrl { get; set; }

        [JsonProperty("mediumProfileImageUrl")]
        public string MediumProfileImageUrl { get; set; }

        [JsonProperty("bigProfileImageUrl")]
        public string BigProfileImageUrl { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("spokenLanguages")]
        public List<string> SpokenLanguages { get; set; }

        [JsonProperty("birthDate")]
        public DateTimeOffset BirthDate { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("interests")]
        public List<Interest> Interests { get; set; }
    }

    public partial class Interest
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("something")]
        public string Something { get; set; }
    }

    public class UpdatedUserProfile
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("spokenLanguages")]
        public List<string> SpokenLanguages { get; set; }

        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("interests")]
        public List<Interest> Interests { get; set; }
    }
}


