﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinStartKit.VIews;

[assembly: ExportFont("Montserrat-Light.ttf", Alias = "FA-Lite")]
[assembly: ExportFont("Montserrat-Medium.ttf", Alias = "FA-Medium")]
[assembly: ExportFont("PassionOne-Regular.ttf", Alias = "FA-Title")]
[assembly: ExportFont("Poppins-ExtraLight.ttf", Alias = "TI-Light")]
[assembly: ExportFont("Poppins-SemiBold.ttf", Alias = "TI-Bold")]
namespace XamarinStartKit
{
    public partial class App : Application
    {
        public static double ScreenHeight;
        public static double ScreenWidth;
        public App(AppSetup setup)
        {
            InitializeComponent();

            AppContainer.Container = setup.CreateContainer();
            var splashScreen = new SpalshScreen();
            MainPage = splashScreen;
            if (IsCurrentUserExsit())
            {
                MainPage = new NavigationPage(new UserMainPage());
            }
            else {
                splashScreen.RefreshEvent += ((s1, e1) =>
                {
                    MainPage = new NavigationPage( new MainPage());
                });
            }

            
        }

        protected bool IsCurrentUserExsit()
        {
            if (Application.Current.Properties.ContainsKey("token"))
            {
                return true;
            }

            return false;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
