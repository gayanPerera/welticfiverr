﻿using System;
namespace XamarinStartKit.Interface
{
    public interface ILoadingIndicator
    {
        void StartIndicator();
        void EndIndicator();
    }
}
