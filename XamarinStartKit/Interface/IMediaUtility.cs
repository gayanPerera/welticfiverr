﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace XamarinStartKit.Interface
{
    public interface IMediaUtility
    {
        Task<String> TakePhotoAsync();
        Task<String> LoadPhotoAsync(FileResult photo);
        Stream GetProfilePicStream();
    }
}
