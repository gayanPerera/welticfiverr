﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinStartKit.Models.ServiceModel;
using XamarinStartKit.Models.UIModel;

namespace XamarinStartKit.Interface.Service
{

    public interface IRestApiReferences
    {
        [Post("/Users/Login")]
        Task<LoginResponse> LoginUser(Login user);

        [Post("/Users/RegisterUser")]
        Task RegisterUser(Register user);

        [Multipart]
        [Post("/Users/UploadProfilePicture")]
        Task UploadProfilePicture(StreamPart stream, [Authorize("Bearer")] string token);

        [Headers("X-Language:EN")]
        [Get("/Interests/GetActions")]
        Task<List<IntrestModel>> GetActions([Authorize("Bearer")] string token);

        [Headers("X-Language:EN")]
        [Get("/Interests/GetSomethings")]
        Task<List<IntrestModel>> GetSomethings([AliasAs("action")] string action, [Authorize("Bearer")] string token);

        [Get("/Users/GetProfile")]
        Task<UserProfile> GetProfile([Authorize("Bearer")] string token);

        [Put("/Users/UpdateProfile")]
        Task UpdateProfile(UserProfileUIModel userProfile, [Authorize("Bearer")] string token);
    }
}
