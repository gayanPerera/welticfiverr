﻿using System;
namespace XamarinStartKit.Interface
{
    public interface IReadFile
    {
        string ReadLocalResourceFile();
    }
}
