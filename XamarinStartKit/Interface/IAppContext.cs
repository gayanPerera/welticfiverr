﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinStartKit.Interface
{
    public interface IAppContext
    {
        void SetData(string id, string data);
        void Removeproperty(string id);
        string Getvalue(string id);
        bool CheckValue(string Id);
        void SavePresitData();
    }
}
