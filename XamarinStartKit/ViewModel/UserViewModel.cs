﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinStartKit.Interface;
using XamarinStartKit.Interface.Service;
using XamarinStartKit.Models.ServiceModel;

namespace XamarinStartKit.ViewModel
{
    public class UserViewModel:BaseViewModel
    {

        private string email;
        private string password;
        private string rePassword;
        private readonly ILoadingIndicator loadingIndicator;
        private readonly IAppContext appContext;

        public UserViewModel(ILoadingIndicator loadingIndicator, IAppContext appContext)
        {
            this.loadingIndicator = loadingIndicator;
            this.appContext = appContext;
        }

        public string Email
        {
            get { return email; }
            set { email = value;
                NotifyPropertyChanged(nameof(Email));
            }
        }

        public string RePassword
        {
            get { return rePassword; }
            set { rePassword = value; }
        }

        public string PassWord
        {
            get { return password; }
            set { 
                password = value;
                NotifyPropertyChanged(nameof(PassWord));
            }
        }

        public async Task<bool> UserAuthontication()
        {
            if (!string.IsNullOrWhiteSpace(Email) & !string.IsNullOrWhiteSpace(PassWord))
            {
                try
                {
                    loadingIndicator.StartIndicator();
                    var currentUser = new Login() { Email = Email, Password = PassWord };
                    var loginResults = await CreateRestClientInstant().LoginUser(currentUser);
                    if (loginResults.Token != null)
                    {
                        appContext.SetData("token", loginResults.Token);
                        loadingIndicator.EndIndicator();
                        return true;
                    }
                    else {
                        loadingIndicator.EndIndicator();
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    loadingIndicator.EndIndicator();
                    return false;
                }
            }
            else
            {
                loadingIndicator.EndIndicator();
                return false;
            }
        }

        public bool CheckPasswordEquls()
        {
           if(string.Equals(RePassword,PassWord))
            {
                return true;
            }
            return false;
        }

        public async Task<bool> CreateNewAccount()
        {
            if (!string.IsNullOrWhiteSpace(Email) & !string.IsNullOrWhiteSpace(PassWord))
            {
                try
                {
                    loadingIndicator.StartIndicator();
                    var newUser = new Register() { Email = Email, Password = PassWord };
                    await CreateRestClientInstant().RegisterUser(newUser);
                    loadingIndicator.EndIndicator();
                    return true;
                }
                catch (Exception ex)
                {
                    loadingIndicator.EndIndicator();
                    return false;
                }
            }
            else
            {
                loadingIndicator.EndIndicator();
                return false;
            }

        }
    }
}
