﻿using AutoMapper;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinStartKit.Interface;
using XamarinStartKit.Models.ServiceModel;
using XamarinStartKit.Models.UIModel;

namespace XamarinStartKit.ViewModel
{
    public class UserProfileSettingsViewModel : BaseViewModel
    {
        private readonly IMediaUtility mediaUtility;
        private readonly ILoadingIndicator loadingIndicator;
        private readonly IAppContext appContext;

        private string userProfile;
        private string userName;
        private string spokenLanuage;
        private string dateOfBirth;
        private string gender;
        private string description;
        private string somethingList;
        private List<InterestUIModle> interests;
        private List<string> actionTranslation;
        private List<string> someThingActionIntrests;


        public UserProfileSettingsViewModel(IMediaUtility mediaUtility,ILoadingIndicator loadingIndicator, IAppContext appContext)
        {
            this.mediaUtility = mediaUtility;
            this.loadingIndicator = loadingIndicator;
            this.appContext = appContext;
            Intrests = new List<InterestUIModle>();
            SomeThingActionIntrests = new List<string>();
            ActionTranslation = new List<string>();
        }

        public string UserProfile
        {
            get { return userProfile; }
            set { userProfile = value;
                NotifyPropertyChanged(nameof(UserProfile));
            }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value;
                NotifyPropertyChanged(nameof(UserName));
            }
        }

        public string SpokenLanuage
        {
            get { return spokenLanuage; }
            set { spokenLanuage = value;
                NotifyPropertyChanged(nameof(SpokenLanuage));
            }
        }

        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value;
                NotifyPropertyChanged(nameof(DateOfBirth));
            }
        }

        public string Gender
        {
            get { return gender; }
            set { gender = value;
                NotifyPropertyChanged(nameof(Gender));
            }
        }

        public string Description
        {
            get { return description; }
            set { description = value;
                NotifyPropertyChanged(nameof(Description));
            }
        }

        public string SomethingList
        {
            get { return somethingList; }
            set
            {
                somethingList = value;
                NotifyPropertyChanged(nameof(SomethingList));
            }
        }

        public List<InterestUIModle> Intrests
        {
            get { return interests; }
            set { interests = value;
                NotifyPropertyChanged(nameof(Intrests));
            }
        }

        public List<string> SomeThingActionIntrests
        {
            get { return someThingActionIntrests; }
            set
            {
                someThingActionIntrests = value;
                NotifyPropertyChanged(nameof(SomeThingActionIntrests));
            }
        }

        public List<string> ActionTranslation
        {
            get { 
                return actionTranslation; 
            }
            set
            {
                actionTranslation = value;
                NotifyPropertyChanged(nameof(ActionTranslation));
            }
        }
        public async Task<string> TakeaPicture()
        {
           return  await mediaUtility.TakePhotoAsync();
        }
        public async void UpdateProfilesAsync( bool profileUpdated)
        {
            loadingIndicator.StartIndicator();
            try 
            {
                if (profileUpdated)
                {
                    var stream = mediaUtility.GetProfilePicStream();
                    await CreateRestClientInstant().UploadProfilePicture(new StreamPart(stream, "photo.png"), appContext.Getvalue("token"));
                }
                var a = SpokenLanuage.Split(',');
                //need to load the inreset
                var currentUserProfile = new UserProfileUIModel()
                {
                    UserName = UserName,
                    SpokenLanguages = SpokenLanuage.Split(',').ToList(),
                    BirthDate = DateOfBirth,
                    Gender = Gender,
                    Description = Description,
                    Interests = Intrests
                };

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(currentUserProfile);
                Console.WriteLine(json);
                await CreateRestClientInstant().UpdateProfile(currentUserProfile, appContext.Getvalue("token"));
                loadingIndicator.EndIndicator();
            }
            catch (Exception ex)
            {
                loadingIndicator.EndIndicator();
            }
        }
        public async Task<bool> GetUserProfile()
        {
            try
            {
                loadingIndicator.StartIndicator();
                var currentUserDetails = await CreateRestClientInstant().GetProfile(appContext.Getvalue("token"));
                UserName = currentUserDetails.UserName;
                SpokenLanuage = String.Join(",", currentUserDetails.SpokenLanguages);
                DateOfBirth = currentUserDetails.BirthDate.DateTime.ToString("yyyy-MM-dd");
                Description = currentUserDetails.Description;
                UserProfile = currentUserDetails.MediumProfileImageUrl;
                Gender = currentUserDetails.Gender;

                var config = new MapperConfiguration(cfg => cfg.CreateMap<Interest, InterestUIModle>());
                var mapper = new Mapper(config);
                Intrests = mapper.Map<List<Interest>, List<InterestUIModle>>(currentUserDetails.Interests);

                foreach (var intrest in Intrests)
                {
                    SomethingList = SomethingList + String.Format(" #{0}", intrest.Something); 
                }

                var getActions = await CreateRestClientInstant().GetActions(appContext.Getvalue("token"));
                ActionTranslation.Clear();
                foreach (var actionTraslation in getActions)
                {
                    ActionTranslation.Add(actionTraslation.Translation);
                }
                var result = ActionTranslation;
                loadingIndicator.EndIndicator();
                return true;
            }
            catch (Exception ex)
            {
                loadingIndicator.EndIndicator();
                return false;
            }
        }
        public async Task<List<string>> GetSomeThingAction(string action)
        {
            try
            {
                loadingIndicator.StartIndicator();
                SomeThingActionIntrests.Clear();
                var result = await CreateRestClientInstant().GetSomethings(action, appContext.Getvalue("token"));
                foreach (var act in result)
                {
                    SomeThingActionIntrests.Add(act.Enum);
                }
                loadingIndicator.EndIndicator();
                return SomeThingActionIntrests;
            }
            catch (Exception e)
            {
                loadingIndicator.EndIndicator();
            }
           return SomeThingActionIntrests;
        }
        public void AddNewIntrest(string action, string something)
        {
            Intrests.Add(new InterestUIModle() { 
            Action = action,
            Something = something
            });

            SomethingList = SomethingList + String.Format(" #{0}", something);
        }
        public bool CheckIntrestIsExsit(string newKey)
        {
            return SomethingList.Contains(newKey);
        }
    }
}
