﻿using Refit;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using XamarinStartKit.Constants;
using XamarinStartKit.Interface.Service;

namespace XamarinStartKit.ViewModel
{
    public class BaseViewModel: INotifyPropertyChanged
    {
        public INavigation navigation;
        public static IRestApiReferences gitHubApi;
        public event PropertyChangedEventHandler PropertyChanged;

        public BaseViewModel()
        {
        }

        public void PushAsync(Page page)
        {
            navigation.PushAsync(page);
        }

        public void PopAsync()
        {
            navigation.PopAsync();
        }
        public IRestApiReferences CreateRestClientInstant()
        {
            if (gitHubApi is null)
            {
                gitHubApi = RestService.For<IRestApiReferences>(Constant.ServiceUrl);
            }

            return gitHubApi;
        }

        public void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
