﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace XamarinStartKit.ViewModel
{
    public class MainViewModel: BaseViewModel
    {
        public MainViewModel()
        {

        }

        public void ClearNavigationStack(Page page)
        {
            foreach (var p in navigation.NavigationStack.ToList())
            {
                if (p != page)
                {
                    navigation.RemovePage(p);
                }
            }
        }
    }
}
