﻿using MLToolkit.Forms.SwipeCardView.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinStartKit.Interface;
using XamarinStartKit.Models.UIModel;

namespace XamarinStartKit.ViewModel
{
   public class TinderUserMainPageViewModel:BaseViewModel
    {
        private ObservableCollection<TinderProfile> _profiles = new ObservableCollection<TinderProfile>();
        private readonly IAppContext appContext;
        private uint _threshold;

        public TinderUserMainPageViewModel(IAppContext appContext)
        {
            InitializeProfiles();
            Threshold = (uint)(App.ScreenWidth / 3);
            SwipedCommand = new Command<SwipedCardEventArgs>(OnSwipedCommand);
            DraggingCommand = new Command<DraggingCardEventArgs>(OnDraggingCommand);
            ClearItemsCommand = new Command(OnClearItemsCommand);
            AddItemsCommand = new Command(OnAddItemsCommand);
            this.appContext = appContext;
        }

        public ObservableCollection<TinderProfile> Profiles
        {
            get => _profiles;
            set
            {
                _profiles = value;
                NotifyPropertyChanged(nameof(Profiles));
            }
        }

        public uint Threshold
        {
            get => _threshold;
            set
            {
                _threshold = value;
                NotifyPropertyChanged(nameof(Threshold));
            }
        }

        public ICommand SwipedCommand { get; }

        public ICommand DraggingCommand { get; }

        public ICommand ClearItemsCommand { get; }

        public ICommand AddItemsCommand { get; }

        public void CleanAllStorages()
        {
            PushAsync(new MainPage());
            appContext.Removeproperty("token");
        }
        private void OnSwipedCommand(SwipedCardEventArgs eventArgs)
        {
        }

        private void OnDraggingCommand(DraggingCardEventArgs eventArgs)
        {
            switch (eventArgs.Position)
            {
                case DraggingCardPosition.Start:
                    return;

                case DraggingCardPosition.UnderThreshold:
                    break;

                case DraggingCardPosition.OverThreshold:
                    break;

                case DraggingCardPosition.FinishedUnderThreshold:
                    return;

                case DraggingCardPosition.FinishedOverThreshold:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnClearItemsCommand()
        {
            Profiles.Clear();
        }

        private void OnAddItemsCommand()
        {
        }

        private void InitializeProfiles()
        {
            Profiles.Add(new TinderProfile { ProfileId = 1, Name = "Laura", Age = 24, Gender = Gender.Female, Photo = "p378674.jpg" });
            Profiles.Add(new TinderProfile { ProfileId = 2, Name = "Sophia", Age = 21, Gender = Gender.Female, Photo = "p327144.jpg" });
            Profiles.Add(new TinderProfile { ProfileId = 3, Name = "Anne", Age = 19, Gender = Gender.Female, Photo = "p378674.jpg" });
            Profiles.Add(new TinderProfile { ProfileId = 4, Name = "Yvonne ", Age = 27, Gender = Gender.Female, Photo = "p327144.jpg" });
            Profiles.Add(new TinderProfile { ProfileId = 5, Name = "Abby", Age = 25, Gender = Gender.Female, Photo = "p378674.jpg" });
        }



       
    }
}
