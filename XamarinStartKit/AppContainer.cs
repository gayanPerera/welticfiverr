﻿using System;
using Autofac;

namespace XamarinStartKit
{
    public class AppContainer
    {
        public static IContainer Container { get; set; }
    }
}
