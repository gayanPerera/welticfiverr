﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinStartKit.ViewModel;

namespace XamarinStartKit.VIews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignupPage : ContentPage
    {
        private UserViewModel vm;
        public SignupPage()
        {
            InitializeComponent();
            vm = AppContainer.Container.Resolve<UserViewModel>();
            BindingContext = vm;
            vm.navigation = Navigation;
        }

        private async void CreateAccountClicked(object sender, EventArgs e)
        {
            var isSamePassowrd = vm.CheckPasswordEquls();
            if(isSamePassowrd)
            {
               var result = await vm.CreateNewAccount();
                if (!result)
                {
                    await DisplayAlert("Account Create", "Something went wrong", "Cancle");
                }
                else {
                    await DisplayAlert("Account Create", $"{vm.Email} Successfully Created", "Cancle");
                }
            }
        }

        private void BackButtonTapped(object sender, EventArgs e)
        {
            vm.PopAsync(); ;
        }
    }
}