﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinStartKit.ViewModel;

namespace XamarinStartKit.VIews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private UserViewModel vm;
        public LoginPage()
        {
            InitializeComponent();
            vm = AppContainer.Container.Resolve<UserViewModel>();
            BindingContext = vm;
            vm.navigation = Navigation;
        }

        private async void LoginBtnClicked(object sender, EventArgs e)
        {
            vm.Email = "gayan@gmail.com";
            vm.PassWord = "1qaz2wsx";
            var status =  await vm.UserAuthontication();

            if(status)
            {
                // await DisplayAlert("Login", "Login Success", "Ok");
                vm.PushAsync(new UserMainPage());
            }
            else
            {
                await DisplayAlert("Login", "Login Failed", "Ok");
            }
        }

        private void BackButtonTapped(object sender, EventArgs e)
        {
            vm.PopAsync();
        }
    }
}