﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinStartKit.VIews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpalshScreen : ContentPage
    {
        public event EventHandler RefreshEvent;
        public SpalshScreen()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            
            await Task.Delay(3500);
            if (RefreshEvent != null)
            {
                RefreshEvent(true, EventArgs.Empty);
            }
        }
    }
}