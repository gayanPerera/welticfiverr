﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinStartKit.Models.ServiceModel;
using XamarinStartKit.ViewModel;

namespace XamarinStartKit.VIews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileEditingPage : ContentPage
    {
        private UserProfileSettingsViewModel vm;
        private bool isProfileUpdated;
        private bool isPageOpen;
        public ProfileEditingPage()
        {
            vm = AppContainer.Container.Resolve<UserProfileSettingsViewModel>();
            BindingContext = vm;
            vm.navigation = Navigation;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (!isPageOpen)
            {
                isPageOpen = true;
                SetupUserProfile();
            }
            base.OnAppearing();
        }
        protected override bool OnBackButtonPressed()
        {
            vm.UpdateProfilesAsync(isProfileUpdated);
            isPageOpen = false;
            isProfileUpdated = false;
            return base.OnBackButtonPressed();
        }
        private async void SetupUserProfile()
        {
           var result =  await vm.GetUserProfile();

            if (result)
            {
                if (vm.Gender.Equals("Male"))
                {
                    male.IsChecked = true;
                }
                else if (vm.Gender.Equals("Female"))
                {
                    female.IsChecked = true;
                }
                else
                {
                    other.IsChecked = true;
                }
            }

            actionPicker.ItemsSource = vm.ActionTranslation;
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
        private void OpenLanuagePicker(object sender, EventArgs e)
        {
           // Device.BeginInvokeOnMainThread(() => { lanuagePicker.Focus(); });
        }
        private void OpenDatePicker(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() => { datepicker.Focus(); });
        }
        private void DatepickerDateSelected(object sender, DateChangedEventArgs e)
        {
           vm.DateOfBirth = datepicker.Date.ToString("yyyy-MM-dd");
        }
        private async void EditImageClick(object sender, EventArgs e)
        {
            isProfileUpdated = true;
            profileImg.Source = await vm.TakeaPicture();
        }

        private void BackTapGestureRecognizer(object sender, EventArgs e)
        {
             vm.UpdateProfilesAsync(isProfileUpdated);
             isPageOpen = false;
             isProfileUpdated = false;
             vm.PopAsync();
        }

        private async void ActionItemSelectedEvent(object sender, EventArgs e)
        {
            var selectedAction = actionPicker.SelectedItem.ToString();
           var result =  await vm.GetSomeThingAction(selectedAction);
            if (sometingPicker.ItemsSource != null)
            {
                sometingPicker.ItemsSource = new List<string>();
            }
            sometingPicker.ItemsSource = result;
        }
        private async void SometingPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            if (sometingPicker.SelectedItem != null)
            {
                var selectedAction = actionPicker.SelectedItem.ToString();
                var sometingAction = sometingPicker.SelectedItem.ToString();

                if (!string.IsNullOrEmpty(selectedAction) && !string.IsNullOrEmpty(sometingAction) && !vm.CheckIntrestIsExsit(sometingAction))
                {
                    vm.AddNewIntrest(selectedAction, sometingAction);
                }
                else
                {
                    await DisplayAlert("Intrest", "Please Select Valid input", "OK");
                }
            }
        }

    }
}