﻿using Autofac;
using System.Linq;
using Xamarin.Forms;
using XamarinStartKit.ViewModel;
using XamarinStartKit.VIews;

namespace XamarinStartKit
{
    public partial class MainPage : ContentPage
    {
        private MainViewModel Vm;
        public MainPage()
        {
            InitializeComponent();
            Vm = AppContainer.Container.Resolve<MainViewModel>();
            Vm.navigation = Navigation;
            BindingContext = Vm;
        }

        protected override void OnAppearing()
        {
            Vm.ClearNavigationStack(this);
            base.OnAppearing();
        }

        private void CreateAccountTapped(object sender, System.EventArgs e)
        {
            Vm.PushAsync(new SignupPage());
        }

        private void LoginWithEmailClicked(object sender, System.EventArgs e)
        {
            Vm.PushAsync(new LoginPage());
        }
    }
}
