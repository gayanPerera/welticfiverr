﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using XamarinStartKit.Interface;

namespace XamarinStartKit.Utiltity
{
   public class AppContext:IAppContext
    {
        public void SetData(string id, string data)
        {
            Application.Current.Properties[id] = data;
        }

        public string Getvalue(string id)
        {
            return Application.Current.Properties[id] as string;
        }

        public void Removeproperty(string id)
        {
            Application.Current.Properties.Remove(id);
        }

        public void SavePresitData()
        {
            Application.Current.SavePropertiesAsync();
        }

        public bool CheckValue(string id)
        {
            if (Application.Current.Properties.ContainsKey(id))
            {
                return true;
            }

            return false;
        }
    }
}
