﻿using System;
using System.IO;
using System.Reflection;
using XamarinStartKit.Interface;

namespace XamarinStartKit.Utiltity
{
    public class ReadFile: IReadFile
    {
        private string readString;
        public ReadFile()
        {
        }

        public string ReadLocalResourceFile()
        {
            try
            {
                var assembly = IntrospectionExtensions.GetTypeInfo(typeof(ReadFile)).Assembly;
                Stream stream = assembly.GetManifestResourceStream("XamarinKit.Resource.SampleTextJson.json");
                using (var reader = new System.IO.StreamReader(stream))
                {
                    readString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

            }
            return readString;
        }
    }
}
