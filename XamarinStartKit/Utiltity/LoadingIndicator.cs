﻿using System;
using Acr.UserDialogs;
using XamarinStartKit.Interface;

namespace XamarinStartKit.Utiltity
{
    public class LoadingIndicator: ILoadingIndicator
    {

        public void StartIndicator()
        {
            UserDialogs.Instance.ShowLoading("Loading", MaskType.Clear);
        }

        public void EndIndicator()
        {
            UserDialogs.Instance.HideLoading();
        }
    }
}
