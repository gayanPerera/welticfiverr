﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using XamarinStartKit.Interface;

namespace XamarinStartKit.Utiltity
{
   public class MediaUtility: IMediaUtility
    {
        private Stream stream;
        public async Task<string> TakePhotoAsync()
        {
            try
            {
                var photo = await MediaPicker.CapturePhotoAsync();
                var image = await LoadPhotoAsync(photo);
                return image;
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Feature is not supported on the device
            }
            catch (PermissionException pEx)
            {
                // Permissions not granted
            }
            catch (Exception ex)
            {
               
            }

            return string.Empty;
        }
        public async Task<String> LoadPhotoAsync(FileResult photo)
        {
            // canceled
            if (photo == null)
            {
                return string.Empty;
             
            }
            // save the file into local storage
            var newFile = Path.Combine(FileSystem.CacheDirectory, photo.FileName);
            stream = await photo.OpenReadAsync();
           // {
                using (var fileStream = File.OpenWrite(newFile))
                {
                    await stream.CopyToAsync(fileStream);
                }
           // }
            return newFile;
        }

        public Stream GetProfilePicStream()
        {
            return stream;
        }
    }
}
